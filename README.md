## Installation

1. Add package:

```sh
$ adonis install https://gitlab.com/hadirajani/adonis-user-acl.git --as adonis-user-acl
```

2. Make sure to register this plugin's provider. The providers are registered inside `start/app.js`.

```js
const providers = [
  ...
  "adonis-user-acl/providers/UserACLProvider"
  ...
]
```

3. Setting up aliases inside `start/app.js` file.

```js
const aliases = {
  ...
  Role: 'UserACL/Models/Role',
  Permission: 'UserACL/Models/Permission',
  ...
}
```

4. Setting up traits to `User` model.

```js
static get traits () {
  return [
    '@provider:UserACL/Models/Traits/HasRole',
    '@provider:UserACL/Models/Traits/HasPermission'
  ]
}
```

5. Setting up middlewares inside `start/kernel.js` file.

```js
const namedMiddleware = {
  ...
  is: 'UserACL/Middlewares/Is',
  can: 'UserACL/Middlewares/Can',
  ...
}
```

## Working With Roles

### Create Role

Lets create roles for your application.

```js
const roleAdmin = new Role();
roleAdmin.name = "Administrator";
roleAdmin.slug = "administrator";
roleAdmin.description = "Manage administration privileges";
await roleAdmin.save();

const roleModerator = new Role();
roleModerator.name = "Moderator";
roleModerator.slug = "moderator";
roleModerator.description = "Manage moderator privileges";
await roleModerator.save();
```

## Attach Role(s) To User

```js
const user = await User.find(1);
await user.roles().attach([roleAdmin.id, roleModerator.id]);
```

### Detach Role(s) From User

```js
const user = await User.find(1);
await user.roles().detach([roleAdmin.id]);
```

### Get User Roles

Get roles assigned to a user.

```js
const user = await User.first();
const roles = await user.getRoles(); // ['administrator', 'moderator']
```

## Working With Permissions

### Create Role Permissions

```js
const createUsersPermission = new Permission();
createUsersPermission.slug = "create_users";
createUsersPermission.name = "Create Users";
createUsersPermission.description = "create users permission";
await createUsersPermission.save();

const updateUsersPermission = new Permission();
updateUsersPermission.slug = "update_users";
updateUsersPermission.name = "Update Users";
updateUsersPermission.description = "update users permission";
await updateUsersPermission.save();

const deleteUsersPermission = new Permission();
deleteUsersPermission.slug = "delete_users";
deleteUsersPermission.name = "Delete Users";
deleteUsersPermission.description = "delete users permission";
await deleteUsersPermission.save();

const readUsersPermission = new Permission();
readUsersPermission.slug = "read_users";
readUsersPermission.name = "Read Users";
readUsersPermission.description = "read users permission";
await readUsersPermission.save();
```

### Attach Permissions to Role

```js
const roleAdmin = await Role.find(1);
await roleAdmin.permissions().attach([createUsersPermission.id, updateUsersPermission.id, deleteUsersPermission.is, readUsersPermission.id]);
```

### Detach Permissions from Role

```js
const roleAdmin = await Role.find(1);
await roleAdmin.permissions().detach([createUsersPermission.id, updateUsersPermission.id, deleteUsersPermission.is, readUsersPermission.id]);
```

### Get User Permissions

Get permissions assigned to a role.

```js
const roleAdmin = await Role.find(1);
// ['create_users', 'update_users', 'delete_users', 'read_users']
await roleAdmin.getPermissions();
```

or

```js
const roleAdmin = await Role.find(1);
// collection of permissions
await roleAdmin.permissions().fetch();
```

## Working With Permissions

### Create User Permissions

```js
const createUsersPermission = new Permission();
createUsersPermission.slug = "create_users";
createUsersPermission.name = "Create Users";
createUsersPermission.description = "create users permission";
await createUsersPermission.save();

const updateUsersPermission = new Permission();
updateUsersPermission.slug = "update_users";
updateUsersPermission.name = "Update Users";
updateUsersPermission.description = "update users permission";
await updateUsersPermission.save();

const deleteUsersPermission = new Permission();
deleteUsersPermission.slug = "delete_users";
deleteUsersPermission.name = "Delete Users";
deleteUsersPermission.description = "delete users permission";
await deleteUsersPermission.save();

const readUsersPermission = new Permission();
readUsersPermission.slug = "read_users";
readUsersPermission.name = "Read Users";
readUsersPermission.description = "read users permission";
await readUsersPermission.save();
```

### Attach Permissions to User

```js
const user = await User.find(1);
await user.permissions().attach([createUsersPermission.id, updateUsersPermission.id, deleteUsersPermission.is, readUsersPermission.id]);
```

### Detach Permissions from User

```js
const user = await User.find(1);
await user.permissions().detach([createUsersPermission.id, updateUsersPermission.id, deleteUsersPermission.is, readUsersPermission.id]);
```

### Get User Permissions

Get permissions assigned to a role.

```js
const user = await User.find(1);
// ['create_users', 'update_users', 'delete_users', 'read_users']
await user.getPermissions();
```

or

```js
const user = await User.find(1);
// collection of permissions
await user.permissions().fetch();
```

## Protect Routes

Syntax:

`and (&&)` - administrator && moderator

`or (||)` - administrator || moderator

`not (!)` - administrator && !moderator

```js
// check roles
Route.get("/users").middleware(["auth:jwt", "is:(administrator || moderator) && !customer"]);

// check permissions
Route.get("/posts").middleware(["auth:jwt", "can:read_posts"]);
```

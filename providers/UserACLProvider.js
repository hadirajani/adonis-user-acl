"use strict";

const { ServiceProvider } = use("@adonisjs/fold");

class UserACLProvider extends ServiceProvider {
  register() {
    this.app.bind("UserACL/Models/Role", () => {
      const Role = require("../src/Models/Role");
      Role._bootIfNotBooted();
      return Role;
    });
    this.app.bind("UserACL/Models/Permission", () => {
      const Permission = require("../src/Models/Permission");
      Permission._bootIfNotBooted();
      return Permission;
    });
    this.app.bind("UserACL/Models/Traits/HasRole", () => {
      const HasRole = require("../src/Models/Traits/HasRole");
      return new HasRole();
    });
    this.app.bind("UserACL/Models/Traits/HasPermission", () => {
      const HasPermission = require("../src/Models/Traits/HasPermission");
      return new HasPermission();
    });
    this.app.bind("UserACL/Middlewares/Is", () => {
      const Is = require("../src/Middlewares/Is");
      return new Is();
    });
    this.app.bind("UserACL/Middlewares/Can", () => {
      const Can = require("../src/Middlewares/Can");
      return new Can();
    });
  }
}

module.exports = UserACLProvider;

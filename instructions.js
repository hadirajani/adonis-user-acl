"use strict";
const path = require("path");

class Instruction {
  constructor(cli) {
    this.cli = cli;
  }

  async execute() {
    let files = [
      "create_role_table",
      "create_role_user_table",
      "create_permission_table",
      "create_permission_role_table",
      "create_permission_user_table"
    ];
    for (let i = 0; i < files.length; i++) {
      await this.generateBlueprint(files[i]);
    }
  }

  async generateBlueprint(name) {
    const templateFile = path.join(__dirname, "./templates", `${name}.mustache`);
    const fileName = `${new Date().getTime()}_${name}.js`;
    const filePath = this.cli.helpers.migrationsPath(fileName);
    await this.cli.copy(templateFile, filePath);
    this.cli.command.completed("create", filePath.replace(this.cli.helpers.appRoot("/"), ""));
  }
}

module.exports = async function(cli) {
  try {
    let instruction = new Instruction(cli);
    await instruction.execute();
  } catch (error) {
    console.error(error);
  }
};

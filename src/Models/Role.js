"use strict";

const Model = use("Adonis/Src/Model");

class Role extends Model {
  permissions(includeDeleted = false) {
    let query = this.belongsToMany("UserACL/Models/Permission");
    if (!includeDeleted) {
      query.whereNull("permissions.deleted_at");
    }
    return query;
  }

  async getPermissions(includeDeleted = false) {
    const permissions = await this.permissions(includeDeleted).fetch();
    return permissions.rows.map(({ slug }) => slug);
  }
}

module.exports = Role;

"use strict";

/**
 * adonis-acl
 * Copyright(c) 2017 Evgeny Razumov
 * MIT Licensed
 */

const _ = require("lodash");
const Acler = require("acler");

module.exports = class HasRole {
  register(Model) {
    Model.prototype.roles = function(includeDeleted = false) {
      let query = this.belongsToMany("UserACL/Models/Role");
      if (!includeDeleted) {
        query.whereNull("roles.deleted_at");
      }
      return query;
    };

    Model.prototype.getRoles = async function(includeDeleted) {
      const roles = await this.roles(includeDeleted).fetch();
      return roles.rows.map(({ slug }) => slug);
    };

    Model.prototype.is = async function(expression) {
      const roles = await this.getRoles();
      return Acler.check(expression, operand => _.includes(roles, operand));
    };
  }
};

"use strict";

const _ = require("lodash");
const Acler = require("acler");

module.exports = class HasPermission {
  register(Model) {
    Model.prototype.permissions = function(includeDeleted = false) {
      let query = this.belongsToMany("UserACL/Models/Permission");
      if (includeDeleted) {
        query.whereNull("permissions.deleted_at");
      }
      return query;
    };

    Model.prototype.getPermissions = async function(includeDeleted = false) {
      let permissions = await this.permissions(includeDeleted).fetch();
      permissions = permissions.rows.map(({ slug }) => slug);
      if (typeof this.roles === "function") {
        let roles = await this.roles(includeDeleted)
          .with("permissions")
          .fetch();
        for (let role of roles.toJSON()) {
          for (let permission of role.permissions) {
            if (permissions.indexOf(permission.slug) < 0) {
              permissions.push(permission.slug);
            }
          }
        }
      }
      return permissions;
    };

    Model.prototype.can = async function(expression) {
      const permissions = await this.getPermissions();
      return Acler.check(expression, operand => _.includes(permissions, operand));
    };
  }
};

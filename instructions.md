## Installation

1. Add package:

```sh
$ adonis install https://gitlab.com/hadirajani/adonis-user-acl.git --as adonis-user-acl
```

2. Make sure to register this plugin's provider. The providers are registered inside `start/app.js`.

```js
const providers = [
  ...
  "adonis-user-acl/providers/UserACLProvider"
  ...
]
```

3. Setting up aliases inside `start/app.js` file.

```js
const aliases = {
  ...
  Role: 'UserACL/Models/Role',
  Permission: 'UserACL/Models/Permission',
  ...
}
```

4. Setting up traits to `User` model.

```js
static get traits () {
  return [
    '@provider:UserACL/Models/Traits/HasRole',
    '@provider:UserACL/Models/Traits/HasPermission'
  ]
}
```

5. Setting up middlewares inside `start/kernel.js` file.

```js
const namedMiddleware = {
  ...
  is: 'UserACL/Middlewares/Is',
  can: 'UserACL/Middlewares/Can',
  ...
}
```
